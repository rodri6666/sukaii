<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Control Panel</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=base_url()?>assets/adminboard/assets/css/custom.css" rel="stylesheet" type="text/css" />
	  <link rel="stylesheet" href="<?=base_url()?>assets/adminboard/css/materialize.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/adminboard/css/material-icons.css">
  </head>
  <?php echo $map['js']; ?>
  <body>
    <!-- github fork banner -->
    <a href="https://github.com/SlimShadyIAm/material-admin"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://camo.githubusercontent.com/38ef81f8aca64bb9a64448d0d70f1308ef5341ab/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6461726b626c75655f3132313632312e706e67" alt="Fork me on GitHub" data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png"></a>

    <ul id="slide-out" class="side-nav fixed z-depth-4">
      <li>
        <div class="userView">
          <div class="background">
            <img src="<?=base_url()?>assets/adminboard/assets/img/photo1.png">
          </div>
          <a href="#!user"><img class="circle" src="<?=base_url()?>assets/adminboard/assets/img/cecilio.png"></a>
          <a href="#!name"><span class="white-text name">Bienvenido</span></a>
          <a href="#!email"><span class="white-text email">Cecilio</span></a>
        </div>
      </li>

    

      

      <li><a class="subheader">ADMINISTRADOR</a></li>
      <li><a href="names.html"><i class="material-icons pink-item">home</i>Inicio</a></li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">Acerca De<i class="material-icons pink-item">supervisor_account</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="userdetails.html">Mision Vision</a></li>
                <li><a href="recentusers.html">Nostros</a></li>
                <li><a href="reports.html">Organigrama</a></li>
                <li><a href="reports.html">Mas</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">Centros De Atención<i class="material-icons pink-item">stars</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?=base_url()?>home/organizacionesf">Organizaciones</a></li>
                <li><a href="<?=base_url()?>home/admin">Asociaciones</a></li>
                <li><a href="<?=base_url()?>home/instif">Instituciones</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">servicios<i class="material-icons pink-item">settings</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="userdetails.html">Legal</a></li>
                <li><a href="recentusers.html">Social</a></li>
                <li><a href="reports.html">Psicologico</a></li>
                <li><a href="reports.html">Salud</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>


      <li><a href="names.html"><i class="material-icons pink-item">people</i>kardex</a></li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">publicacion<i class="material-icons pink-item">public</i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="userdetails.html">Publicaciones</a></li>
                <li><a href="recentusers.html">Libros</a></li>
                <li><a href="reports.html">Videos</a></li>
                <li><a href="reports.html">Fotos</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>






  
    </ul>
    
    <main>

    <section class="content">
      <div class="page-announce valign-wrapper"><a href="#" data-activates="slide-out" class="button-collapse valign hide-on-large-only"><i class="material-icons">menu</i></a><h1 class="page-announce-text valign">MENU PRINCIPAL </h1></div>
      <!-- Stat Boxes -->
   
        </section>
        </main>
		