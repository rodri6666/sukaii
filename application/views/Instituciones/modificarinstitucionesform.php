<main>
    
<div class="container">
<?php
foreach ($instituciones->result() as $row) {
?>
<?php echo form_open_multipart('home/modificardbInsti'); ?>
<input type="hidden" name="idinstituciones" value="<?php echo $row->idinstituciones; ?>"></input>
<input type="text" name="tipo" value="<?php echo $row->tipo; ?>"></input>
<input type="text" name="nombre" value="<?php echo $row->nombre; ?>"></input>
<input type="text" name="distrito" value="<?php echo $row->distrito; ?>"></input>
<input type="text" name="direccion" value="<?php echo $row->direccion; ?>"></input>
<input type="text" name="telefono" value="<?php echo $row->telefono; ?>"></input>
<input type="text" name="correo" value="<?php echo $row->correo; ?>"></input>
<input type="text" name="codigoPostal" value="<?php echo $row->codigoPostal; ?>"></input>
<input type="text" name="nroDomicilio" value="<?php echo $row->nroDomicilio; ?>"></input>
<input type="text" name="fax" value="<?php echo $row->fax; ?>"></input>
<input type="text" name="paginaWeb" value="<?php echo $row->paginaWeb; ?>"></input>
<input type="text" name="mision" value="<?php echo $row->mision; ?>"></input>
<input type="text" name="vision" value="<?php echo $row->vision; ?>"></input>
<input type="text" name="publico" value="<?php echo $row->publico; ?>"></input>
<button class="btn waves-effect waves-light" type="submit" name="action">Editar
<i class="material-icons right">edit</i></button>
<?php echo form_close(); ?>
<?php
}
?>
</div>
</main>