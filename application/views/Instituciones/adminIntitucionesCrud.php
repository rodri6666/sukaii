<main>
<table class="responsive-table">
    <thead>
        <tr>
            <th>Tipo</th>
            <th>nombre</th>
            <th>distrito</th>
            <th>direccion</th>
            <th>telefono</th>
            <th>correo</th>
            <th>codigoPostal</th>
            <th>nroDomicilio</th>
            <th>fax</th>
            <th>paginaWeb</th>
            <th>mision</th>
            <th>vision</th>
            <th>objetivos</th>
            <th>publico</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $indice=1;
        foreach ($instituciones ->result() as $row)  {
        ?>
        <tr>
        <td><?php echo $indice; ?></td>
        <td><?php echo $row->tipo; ?></td>
        <td><?php echo $row->nombre; ?></td>
        <td><?php echo $row->distrito; ?></td>
        <td><?php echo $row->direccion; ?></td>
        <td><?php echo $row->telefono; ?></td>
        <td><?php echo $row->correo; ?></td>
        <td><?php echo $row->codigoPostal; ?></td>
        <td><?php echo $row->nroDomicilio; ?></td>
        <td><?php echo $row->fax; ?></td>
        <td><?php echo $row->paginaWeb; ?></td>
        <td><?php echo $row->mision; ?></td>
        <td><?php echo $row->vision; ?></td>
        <td><?php echo $row->objetivos; ?></td>
        <td><?php echo $row->publico; ?></td>
    <td>
    <?php echo form_open_multipart('home/modificarInsti'); ?>
    <input type="hidden" name="idinstituciones" value="<?php echo $row->idinstituciones; ?>"></input>
    <button class="btn waves-effect waves-light" type="submit" name="action">Editar
    <i class="material-icons right">edit</i></button>
    <?php echo form_close(); ?>
    </td>
            <td>
                    <?php echo form_open_multipart('home/eliminardbInsti'); ?>
    <input type="hidden" name="idinstituciones" value="<?php echo $row->idinstituciones; ?>"></input>
    <input type="hidden" name="tipo" value="<?php echo $row->tipo; ?>"></input>
    <button type="submit" class="btn btn-primary">Eliminar
    <i class="material-icons right">delete</i></button>
    <?php echo form_close(); ?>
    </td>
        </tr>
        <?php
        $indice++;
        }
        ?>
    </tbody>
</table>    
</main>