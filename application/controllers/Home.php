<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('googlemaps'));
        $this->load->model('Model_organizacion');
        $this->load->model('Model_asociaciones');
        $this->load->model('Model_instituciones');
        
        
    }
    

    public function index()
    {
        $this->load->view('header');
		 $data['map']= $this->googlemaps->create_map();
        $this->load->view('v_home',$data);
        $this->load->view('footer');
    }
    public function organizacionesf()
    {
       
        $this->load->library('googlemaps');
            $config['center']= '-17.393487, -66.157695';
            $config['zoom']='15';
            $this->googlemaps->initialize($config);
            $marker['position']='-17.393487, -66.157695';
            
            $marker['draggable']= true;
            $marker['ondragend']='setMapToForm(event.latLng.lat(),event.latLng.lng());';
            $this->googlemaps->add_marker($marker);

            
            //validacion input

            $valid=$this->form_validation;
            $valid->set_rules('tipo','nombre','required', array('required' =>'%s Harus Diisi'));
    
            if ($valid->run()==FALSE) {
    
                $data['map']= $this->googlemaps->create_map();
                $this->load->view('v_organizaciones',$data);
            }
            else 
            
            {
                $i=$this->input;
                $data = array('tipo'=> $i->post('tipo'),
                                'nombre'=> $i->post('nombre'),
                                'distrito'=> $i->post('distrito'),
                                'direccion'=> $i->post('direccion'),
                                'telefono'=> $i->post('telefono'),
                                'correo'=> $i->post('correo'),
                                'codigoPostal'=> $i->post('codigoPostal'),
                                'nroDomicilio'=> $i->post('nroDomicilio'),
                                'fax'=> $i->post('fax'),
                                'paginaWeb'=> $i->post('paginaWeb'),
                                'mision'=> $i->post('mision'),
                                'vision'=> $i->post('vision'),
                                'objetivos'=> $i->post('objetivos'),
                                'latitude'=> $i->post('latitude'),
                                'longitude'=> $i->post('longitude'),
                                'publico'=> $i->post('publico'),
                             );

                             $this->Model_organizacion->tambah($data);
                             $this->session->set_flashdata('sukses', 'Se registro con existo!');
                             
                             redirect(base_url('home/organizacionesf'),'refresh');
            }


    
        $this->load->view('admin/footer_admin');
    }

    public function organizacion()
    {
        $this->load->view('header');
        $data['organizacion']=$this->Model_organizacion->retornarOrganizacion();
        $this->load->view('v_organizacion',$data);
        $this->load->view('footer');
            
    }

    public function asociacionesf()
    {
        $this->load->view('header');
        $data['map']= $this->googlemaps->create_map();
        $data['asociaciones']=$this->Model_asociaciones->retornarAsociaciones();
        $this->load->view('admin/v_asociaciones_Admin',$data);
        $this->load->view('footer');
    }
        public function modificar()
        {
        $idasociaciones=$_POST['idasociaciones'];
        $data['asociaciones']=$this->Model_asociaciones->recuperarasociaciones($idasociaciones);
        $this->load->view('header');
        $this->load->view('admin/modificarasociacionesform',$data);
        $this->load->view('footer');
        }
        public function modificardb()
        {
        $idasociaciones=$_POST['idasociaciones'];
        $tipo=$_POST['tipo'];
        $data['tipo']=$tipo;

        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;

        $distrito=$_POST['distrito'];
        $data['distrito']=$distrito;

        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;

        $telefono=$_POST['telefono'];
        $data['telefono']=$telefono;

        $correo=$_POST['correo'];
        $data['correo']=$correo;

        $codigoPostal=$_POST['codigoPostal'];
        $data['codigoPostal']=$codigoPostal;

        $nroDomicilio=$_POST['nroDomicilio'];
        $data['nroDomicilio']=$nroDomicilio;

        $fax=$_POST['fax'];
        $data['fax']=$fax;

        $paginaWeb=$_POST['paginaWeb'];
        $data['paginaWeb']=$paginaWeb;

        $mision=$_POST['mision'];
        $data['mision']=$mision;

        $vision=$_POST['vision'];
        $data['vision']=$vision;

        $objetivos=$_POST['objetivos'];
        $data['objetivos']=$objetivos;

        $this->Model_asociaciones->modificarasociaciones($idasociaciones,$data);
        $this->load->view('header');
        redirect(base_url('home/asociacionesf'),'refresh');
        $this->load->view('footer');
        
    }

    public function eliminardb()
    {
    $idasociaciones=$_POST['idasociaciones'];
    $tipo=$_POST['tipo'];
    $data['tipo']=$tipo;
    $this->Model_asociaciones->eliminarAsociaciones($idasociaciones);
    redirect(base_url('home/asociacionesf'),'refresh');
    }

    public function agregar()
    {
        
    $this->load->view('header');
    $this->load->view('admin/agregarformAsociaciones');
    $this->load->view('footer');


    
    }
    public function agregardb()
    {
    $asociaciones=$_POST['asociaciones'];
    $data['asociaciones']=$asociaciones;
    $this->Model_asociaciones->agregarAsociaciones($data);
    $this->load->view('head');
    $this->load->view('agregarmensaje',$data);
    $this->load->view('footer');
    }

    public function admin()
    {
        $this->load->library('googlemaps');
            $config['center']= '2.984473, 99.628812';
            $config['zoom']='15';
            $this->googlemaps->initialize($config);
            $marker['position']='2.984473, 99.628812';
            $marker['draggable']= true;
            $marker['ondragend']='setMapToForm(event.latLng.lat(),event.latLng.lng());';
            $this->googlemaps->add_marker($marker);

            
            //validacion input

            $valid=$this->form_validation;
            $valid->set_rules('tipo','nombre','required', array('required' =>'%s Harus Diisi'));
    
            if ($valid->run()==FALSE) {
    
                $data['map']= $this->googlemaps->create_map();
                $this->load->view('adminAsociaciones',$data);
            }
            else 
            
            {
                $i=$this->input;
                $data = array('tipo'=> $i->post('tipo'),
                                'nombre'=> $i->post('nombre'),
                                'distrito'=> $i->post('distrito'),
                                'direccion'=> $i->post('direccion'),
                                'telefono'=> $i->post('telefono'),
                                'correo'=> $i->post('correo'),
                                'codigoPostal'=> $i->post('codigoPostal'),
                                'nroDomicilio'=> $i->post('nroDomicilio'),
                                'fax'=> $i->post('fax'),
                                'paginaWeb'=> $i->post('paginaWeb'),
                                'mision'=> $i->post('mision'),
                                'vision'=> $i->post('vision'),
                                'objetivos'=> $i->post('objetivos'),
                                'latitude'=> $i->post('latitude'),
                                'longitude'=> $i->post('longitude'),
                                'publico'=> $i->post('publico'),
                             );

                             $this->Model_asociaciones->agregarAsociaciones($data);
                             $this->session->set_flashdata('sukses', 'Se registro con existo!');
                             
                             redirect(base_url('home/admin'),'refresh');
            }


    
        $this->load->view('admin/footer_admin');
        
        
    }


    //________________________________________________
    // AQUI REGISTRAMOS LAS INTITUCIONES CON EL MAPEO
    public function instif()
    {
        $this->load->library('googlemaps');
        $config['center']= '-17.393487, -66.157695';
        $config['zoom']='15';
        $this->googlemaps->initialize($config);
        $marker['position']='-17.393487, -66.157695';
        $marker['draggable']= true;
        $marker['ondragend']='setMapToForm(event.latLng.lat(),event.latLng.lng());';
        $this->googlemaps->add_marker($marker);

        
        //validacion input

        $valid=$this->form_validation;
        $valid->set_rules('tipo','nombre','required', array('required' =>'%s Harus Diisi'));

        if ($valid->run()==FALSE) {

            $data['map']= $this->googlemaps->create_map();
            $this->load->view('admin_Instituciones',$data);
        }
        else 
        
        {
            $i=$this->input;
            $data = array('tipo'=> $i->post('tipo'),
                            'nombre'=> $i->post('nombre'),
                            'distrito'=> $i->post('distrito'),
                            'direccion'=> $i->post('direccion'),
                            'telefono'=> $i->post('telefono'),
                            'correo'=> $i->post('correo'),
                            'codigoPostal'=> $i->post('codigoPostal'),
                            'nroDomicilio'=> $i->post('nroDomicilio'),
                            'fax'=> $i->post('fax'),
                            'paginaWeb'=> $i->post('paginaWeb'),
                            'mision'=> $i->post('mision'),
                            'vision'=> $i->post('vision'),
                            'objetivos'=> $i->post('objetivos'),
                            'latitude'=> $i->post('latitude'),
                            'longitude'=> $i->post('longitude'),
                            'publico'=> $i->post('publico'),
                         );

                         $this->Model_instituciones->agregarInstituciones($data);
                         $this->session->set_flashdata('sukses', 'Se registro con existo!');
                         
                         redirect(base_url('home/instif'),'refresh');

                         
        }



    $this->load->view('admin/footer_admin');
    
        
    }

    public function insitucionesCRUD()
    {
        $this->load->view('admin/header_AdminCRUD');
        
        $data['instituciones']=$this->Model_instituciones->retornarInstituciones();
        $this->load->view('Instituciones/adminIntitucionesCrud', $data);
     
        
    }

    public function insitucionesCRUDs()
    {
        $this->load->view('header');
        
        $data['instituciones']=$this->Model_instituciones->retornarInstituciones();
        $this->load->view('Instituciones/v_Instituciones',$data);
        
    }

    public function modificarInsti()
    {
        $this->load->view('admin/header_AdminCRUD');
    $idinstituciones=$_POST['idinstituciones'];
    $data['instituciones']=$this->Model_instituciones->recuperarinstituciones($idinstituciones);
    $this->load->view('Instituciones/modificarinstitucionesform',$data);
    }
    public function modificardbInsti()
    {
    $idinstituciones=$_POST['idinstituciones'];
    $tipo=$_POST['tipo'];
    $data['tipo']=$tipo;

    $nombre=$_POST['nombre'];
    $data['nombre']=$nombre;

    $distrito=$_POST['distrito'];
    $data['distrito']=$distrito;

    $direccion=$_POST['direccion'];
    $data['direccion']=$direccion;

    $telefono=$_POST['telefono'];
    $data['telefono']=$telefono;

    $correo=$_POST['correo'];
    $data['correo']=$correo;

    $codigoPostal=$_POST['codigoPostal'];
    $data['codigoPostal']=$codigoPostal;

    $nroDomicilio=$_POST['nroDomicilio'];
    $data['nroDomicilio']=$nroDomicilio;

    $fax=$_POST['fax'];
    $data['fax']=$fax;

    $paginaWeb=$_POST['paginaWeb'];
    $data['paginaWeb']=$paginaWeb;

    $mision=$_POST['mision'];
    $data['mision']=$mision;

    $vision=$_POST['vision'];
    $data['vision']=$vision;

    $objetivos=$_POST['objetivos'];
    $data['objetivos']=$objetivos;
    $this->Model_instituciones->modificarinstituciones($idinstituciones,$data);
    redirect(base_url('home/insitucionesCRUD'),'refresh');
    
    }
    
    public function eliminardbInsti()
    {
    $idinstituciones=$_POST['idinstituciones'];
    $tipo=$_POST['tipo'];
    $data['tipo']=$tipo;


    $this->Model_instituciones->eliminarInstituto($idinstituciones);
    redirect(base_url('home/insitucionesCRUD'),'refresh');
    }


 
    
    

}

/* End of file Home.php */

?>