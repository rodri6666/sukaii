<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_asociaciones extends CI_Model {


    public function agregarAsociaciones($data)
    {
         $this->db->insert('asociaciones',$data);
    }
  

    public function retornarAsociaciones()
    {
          $this->db->select('*');
        $this->db->from('asociaciones');
        return $this->db->get();
    }
    public function recuperarasociaciones ($idasociaciones)
    {
    $this->db->select('*');
    $this->db->from('asociaciones');
    $this->db->where('idasociaciones',$idasociaciones);
    return $this->db->get();
    }
    public function  modificarasociaciones($idasociaciones,$data)
    {
    $this->db->where('idasociaciones',$idasociaciones);
    $this->db->update('asociaciones',$data);
    }
    public function eliminarAsociaciones($idasociaciones)
    {
    $this->db->where('idasociaciones',$idasociaciones);
    $this->db->delete('asociaciones');
    }

    

}

/* End of file Model_asociaciones.php */

?>