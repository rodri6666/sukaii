<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_instituciones extends CI_Model {

    //insert
    public function agregarInstituciones($data)
    {
         $this->db->insert('instituciones',$data);
    }
//select
    public function retornarInstituciones()
    {
          $this->db->select('*');
        $this->db->from('instituciones');
        return $this->db->get();
    }
    //modificar
    public function recuperarinstituciones ($idinstituciones)
    {
    $this->db->select('*');
    $this->db->from('instituciones');
    $this->db->where('idinstituciones',$idinstituciones);
    return $this->db->get();
    }
    public function  modificarinstituciones($idinstituciones,$data)
    {
    $this->db->where('idinstituciones',$idinstituciones);
    $this->db->update('instituciones',$data);
    }
//eliminar
    public function eliminarInstituto($idinstituciones)
    {
    $this->db->where('idinstituciones',$idinstituciones);
    $this->db->delete('instituciones');
    }

}

/* End of file Model_instituciones.php */

?>