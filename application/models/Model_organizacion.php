<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_organizacion extends CI_Model {

    public function tambah($data)
    {
        $this->db->insert('organizacion',$data);
    }

    public function retornarOrganizacion()
    {
          $this->db->select('*');
        $this->db->from('organizacion');
        return $this->db->get();
    }
    
}

/* End of file M_gis.php */

?>